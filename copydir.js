var copydir = require('copy-dir');
copydir.sync('/src/css/', '/dist/css/');
copydir.sync('/src/img/', '/dist/img/');
copydir.sync('/src/lib/', '/dist/lib/');
copydir.sync('/src/font/', '/dist/font/');