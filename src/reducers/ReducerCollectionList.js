export default function(state = null, action){
	switch (action.type){
		case 'REQUEST_COLLECTION_RECOMMEND':
			return recommend(action.data,action.ids);
		case 'REQUEST_COLLECTION_BY_SEARCH':
			return search(action.data,action.keypack);
	}
	return state;
}
function search(data,keypack){
	if(keypack){
		var key_arry = Object.keys(keypack);
		var tmpcollect = [] ;
		for(var a in data){
			
			if(checkkeys(data[a],keypack)){
				tmpcollect.push(data[a]);
			}
		}
		return tmpcollect;
	}else{
		return [];
	}
}
function checkkeys(data,keypack){
	for(var v in keypack){
		if(data[v] instanceof Array){
			var has_one = false;
			for(var i = 0; i<data[v].length;i++){
				if(data[v][i]==keypack[v]||keypack[v]=='all'){has_one=true;}
			}
			if(!has_one){return false;}
		}else{
		if(data[v]!=keypack[v]&&keypack[v]!='all'){return false;}
		}
	}
	return true;
}
function recommend(data,ids){
	if(ids){
		var tmparry=[];
		ids.forEach(function(index){
			if(data[index]){
				tmparry.push(data[index]);
			}
		});
		return tmparry;
	}else{
		return [];
	}	
}
