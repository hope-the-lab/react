export default function(state = null, action){
	if(action.type=='REQUEST_POP_UP'){
		return action.data;
	}
	return state;
}
