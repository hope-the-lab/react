export default function(state = null, action){
	if(action.type=='SET_COMPANY_INFO'){
		if(company[action.company]){
			return company[action.company];
		}else{
			return null;
		}
	}
	return state;
}
const company ={
	tpl:{
		id:'tpl',
		name:"Top Link INT'L Exhibition Co.Ltd",
		name_ch:'上聯國際展覽有限公司',
		img:'tpl_logo.png',
		description:'上聯國際展覽公司成立於1994年，是國內展覽規模第一大的專業展覽公司。<br />主要服務項目為承辦國內外各類型展覽，及展覽徵展業務、議題創意、媒體公關、展覽顧問、整合行銷規劃、國際策展交流等。',
		product:['k026','k027'],
	},
	jetgo:{
		id:'jetgo',
		name:"Jet-Go Consulting Group Public Relations",
		name_ch:'戰國策國際顧問有限公司',
	},
	cyo:{
		id:'cyo',
		name:"CHUANG YAO Design Co.Ltd",
		name_ch:'創耀行銷設計有限公司',
	},
	rocn:{
		id:'rocn',
		name:"The Republic of China Navy",
		name_ch:'中華民國海軍',
		product:['k017'],
	},
	udesign:{
		id:'udesign',
		name:'U Create Media Technology Co.Ltd',
		name_ch:'優創媒體科技有限公司',
	},
	ntcudct:{
		id:'ntcudct',
		name:'National Taichung University Digital Content Technolegy',
		name_ch:'台中教育大學 數位內容科技學系',
		description:'台中教育大學 數位內容科技學系',
		img:'ntcudct.jpg',
		product:['k001','k010'],
	},
}