import { combineReducers } from 'redux';
import ReducerConnact from './ReducerConnact';
import ReducerCareerList from './ReducerCareerList';
import ReducerCareerInfo from './ReducerCareerInfo';
import ReducerCompaneyInfo from './ReducerCompaneyInfo';
import ReducerSkillData from './ReducerSkillData'; 
import ReducerSkillIcons from './ReducerSkillIcons';
import ReducerCollectionList from './ReducerCollectionList';
import ReducerCollectionData from './ReducerCollectionData'; 
import ReducerCollectSreachBar from './ReducerCollectSreachBar'; 
import ReducerCollectPopUp from './ReducerCollectPopUp'; 

const rootReducer = combineReducers({
	contact:ReducerConnact,
	careerList:ReducerCareerList,
	careerInfo:ReducerCareerInfo,
	companyInfo:ReducerCompaneyInfo,
	SkillData:ReducerSkillData,
	skillIcon:ReducerSkillIcons,
	collectionList:ReducerCollectionList,
	collectData:ReducerCollectionData,
	collectSreachBar:ReducerCollectSreachBar,
	collectPopUp:ReducerCollectPopUp,
});

export default rootReducer;
