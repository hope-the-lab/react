export default function(state = null, action){
	if(action.type=='SET_CAREER_INFO'){
		return action.info;
	}
	return state;
}