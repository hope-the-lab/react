export default function(state = [], action){
	if(action.type=='REQUEST_SKILL_ICON'){
		return packIcons(action.data,action.icons);
	}
	return state;
}

function packIcons(data,array){
	var tmparry = [];
	if(array){
			array.map(function(a){
			if(data[a]){
				tmparry.push(data[a]);
			}
		});
	}
	return tmparry;
}