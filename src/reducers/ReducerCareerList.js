export default function(state = null, action){
	if(action.type=='REQUEST_CAREER_LIST'){
		return data[action.userId];
	}
	return state;
}
const data ={
		kaoru:[
			{
				title:'Jet-Go Consulting Group Public Relations',
				company:'jetgo',
				startYears:'2010',
				endYears:'2012',
				experience:['ps','ai'],
				jobTitle:'Summer intern',
			},
			{
				title:'CHUANG YAO Design Co.Ltd',
				company:'cyo',
				startYears:'2014',
				endYears:'2014',
				experience:['css3','html5','jquery'],
				jobTitle:'Intern',
			},
			{
				title:'NTCU Digital Content Technolegy',
				company:'ntcudct',
				startYears:'2011',
				endYears:'2014',
				experience:['ai','ps','html5','sw','maya','android'],
				jobTitle:'college student',
			},
			{
				title:'U Create Media Technology Co.Ltd',
				company:'udesign',
				startYears:'2014',
				endYears:'2014',
				experience:['css3','html5','jquery'],
				jobTitle:'Web Designer',

			},
			{
				title:'ROC Navy',
				company:'rocn',
				startYears:'2014',
				endYears:'2015',
				experience:['pr'],
				jobTitle:"Gunner's Mate (mandatory military service)",

			},
			{
				title:'Fishackathon Tapei (Special Innovation Award)',
				company:'ft',
				endYears:'2016',
				experience:['ai','jquery','rwd','html5','css3'],
				description:"<a href='http://www.fishackathon.co/wrapup-taipei/' target='_blank'>http://www.fishackathon.co/wrapup-taipei/</a>"
			},
			{
				title:"Top Link INT'L Exhibition Co.Ltd",
				company:'tpl',
				productTag:'tpl',
				startYears:'2015',
				endYears:'2017',
				jobTitle:'Front-end Developer',
				experience:['ps','ai','git','js','jquery','rwd','html5','css3','angular','react'],
				description:'Stucture all top-link Website front-end part and some art work.We built at least 5 website per month, and 80 website for each year.',
			}
		]
}
const company ={
	tpl:{
		name:"Top Link INT'L Exhibition Co.Ltd",
		name_ch:'上聯國際展覽有限公司',
		img:'tpl_logo.png',
		description_ch:'上聯國際展覽公司成立於1994年，是國內展覽規模第一大的專業展覽公司。<br />主要服務項目為承辦國內外各類型展覽，及展覽徵展業務、議題創意、媒體公關、展覽顧問、整合行銷規劃、國際策展交流等。',
	},
	rocn:{
		name:"The Republic of China Navy",
		name_ch:'中華民國海軍',
	},
	udesign:{
		name:'U Create Media Technology Co.Ltd',
		name_ch:'優創媒體科技有限公司',
	},
	ntcudct:{
		name:'National Taichung University Digital Content Technolegy',
		name_ch:'台中教育大學 數位內容科技學系',
		img:'ntcudct.jpg',
	},
}