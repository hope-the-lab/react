export default function(state = null){
	state = data;
	return state;
}
const data ={
	ai:{id:'ai',name:'illustrator',img:'ai.png',level:'1'},
	android:{id:'android',name:'Android(java)',img:'android.png',level:'0'},
	angular:{id:'angular',name:'Angular JS',img:'angular.png',level:'0'},
	api:{id:'api',name:'Resful API',img:'api.png',level:'1'},
	ci:{id:'ci',name:'Codeigniter',img:'ci.png',level:'0'},
	css3:{id:'css3',name:'CSS3',img:'css3.png',level:'1'},
	git:{id:'git',name:'Git',img:'git.png',level:'1'},
	html5:{id:'html5',name:'Html5',img:'html5.png',level:'1'},
	jquery:{id:'jquery',name:'Jquery',img:'jquery.png',level:'1'},
	js:{id:'js',name:'Javascript',img:'js.png',level:'1'},
	mongo:{id:'mongo',name:'mongoDB',img:'mongo.png',level:'0'},
	maya:{id:'maya',name:'Maya',img:'maya.png',level:'0'},
	node:{id:'node',name:'Node JS',img:'node.png',level:'1'},
	ps:{id:'ps',name:'Photoshops',img:'ps.png',level:'1'},
	react:{id:'react',name:'React JS',img:'react.png',level:'1'},
	reactnative:{id:'reactnative',name:'React Nactive',img:'react.png',level:'0'},
	rwd:{id:'rwd',name:'Responsive Web Design',img:'rwd.png',level:'1'},
	sw:{id:'sw',name:'SolidWork',img:'sw.png',level:'0'},
	ae:{id:'ae',name:'After Effect',img:'ae.png',level:'0'},
	pr:{id:'pr',name:'Premiere',img:'pr.png',level:'0'},
	au:{id:'au',name:'Adobe Audition',img:'au.png',level:'0'},
	xtion:{id:'xtion',name:'Asus Xtion Pro',img:'xtion.png',level:'0'},
}