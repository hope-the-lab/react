import React from 'react';
import { connect } from 'react-redux';
import { requestContactIcon } from '../../../actions/index';
import { bindActionCreators } from 'redux';

class ContactIocnList extends React.Component {
  constructor(props) {
    super(props);
  }
  componentWillMount() {
    this.props.contactRequest('kaoru');
  }
  renderIcon(){
    if(this.props.contact){
      return this.props.contact.map((item)=>{
        return (
          <li key={item.name}><a href={item.url} target="_blank"><img alt={item.name} src={'img/contact/'+item.path} /></a></li>
          )
      });
    }else{
      return(
        <li>connecting..</li>
        )
    }
  }
  render() {
    return (
        <ul>
          {this.renderIcon()}
        </ul>
      );
    }
  }
function mapStateToProps(state) {
  // Whatever is returned will show up as props
  // inside of BookList
  return {
    contact: state.contact
  };
}
function mapDispatchToProps(dispatch){
  return bindActionCreators({ contactRequest: requestContactIcon }, dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps)(ContactIocnList);