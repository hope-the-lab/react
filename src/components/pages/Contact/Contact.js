import React from 'react';
import ContactIocnList from './ContactIocnList'

export default class Contact extends React.Component {
  render() {
    return (
          <section className="profile_contact" style={{minHeight:this.props.w_height}}>
            <div className="leftsidepadding"></div>
            <div className="content">
              <div>
                <h2>Contact</h2>
                <ContactIocnList />
              </div>
            </div>
            <div className="rightsidepadding"></div>
          </section>
    );
  }
}
