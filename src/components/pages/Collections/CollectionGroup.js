import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {requestPopUp} from '../../../actions/index';
import myEvent from '../../../actions/event';

class CollectionGroup extends React.Component {
  constructor(props) {
    super(props);
    this.clickHandler = this.clickHandler.bind(this);
  }
  upperCaseFirstLetter(string){
    return string.charAt(0).toUpperCase() + string.slice(1);
  }
  clickHandler(data){
    this.props.requestPopUp(data);
    myEvent.emit('showPop');
  }
  renderList(){
    var base = this;
    return (
      this.props.list.map(function(item){
        return(
          <div key={item.name} className="col_1_5">
            <div className="collections_item" onClick={()=>{base.clickHandler(item)}} style={{backgroundImage:'url(img/'+item.artist_id+'/collection/'+item.tmb_img+')'}}>
              <div className="layout">
                <label>{item.kind}</label>
              </div>
            </div>
          </div>
        )
      })
    )
  }
  render() {
    return (
      <div>
        <h2>{this.upperCaseFirstLetter(this.props.title)}</h2>
                <div className="clearfix">
                  {this.renderList()}
                </div>
      </div>
    );
  }
}
function mapDispatcher(dispatcher){
  return bindActionCreators({requestPopUp},dispatcher)
}
export default connect(null,mapDispatcher)(CollectionGroup);