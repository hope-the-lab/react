import React from 'react';
import CollectionGroup from './CollectionGroup';
import {connect} from 'react-redux';
import myEvent from '../../../actions/event';
class Collections extends React.Component {
  constructor(props) {
    super(props);
    this.sendReset=this.sendReset.bind(this);
  }
  componentDidMount() {
  }
  sortTheState(list){
    var kinderObj = {};
    list.map(function(item){
      if(!kinderObj[item.kind]){
        kinderObj[item.kind] = [];
        kinderObj[item.kind].push(item);
      }else{
        kinderObj[item.kind].push(item);
      }
    });
    return kinderObj;
  }
  componentDidUpdate(prevProps, prevState) {
    myEvent.emit('reTotalHeight');
  }
  sendReset(){
    myEvent.emit('searchReset');
  }
  grouper(kinderObj){
    return(
        Object.keys(kinderObj).map(function(index){
          return(
            <CollectionGroup key={index} title={index} list={kinderObj[index]} />
          )
        })
      )
  }
  render_group(){
    if(this.props.List.length>0){
      var kinderObj = this.sortTheState(this.props.List);
      return this.grouper(kinderObj);
    }else{
      return(
        <div className="no_result">
          <h3 onClick={()=>this.sendReset()}>could not found any result, click here to see all stuff</h3>
        </div>
      )
    }
  }
  render() {
    return (
      <section className="profile_collections" style={{minHeight:this.props.w_height}}>
            <div className="leftsidepadding"></div>
            <div className="content">
                {this.render_group()}
            </div>
            <div className="rightsidepadding"></div>
          </section>
    );
  }
}

function mapState(state){
  return{
    List:state.collectionList,
  }
}
export default connect(mapState)(Collections)