import React from 'react';
import {connect} from 'react-redux';
import {requestSkillIcons} from '../../../actions/index';
import {bindActionCreators} from 'redux';
import Icon from '../Icon'

class ExpIcons extends React.Component {
  translateHtml(string){
    return({__html:string});
  }
  renderIcons(arry){
      if(arry){
        return(
          arry.map((icon)=>{
            return (<Icon alt={icon.name} key={icon.name} id={icon.id} level={icon.level} src={"img/icons/"+icon.img} />)
          })
        )
      }
    }
  render() {
    return (
            <li>
            {(()=> {if(this.props.skillIcon.length>0){
              return(
              <ul>
                <li>Experience</li>
                <li>
                {this.renderIcons(this.props.skillIcon)}
                </li>
              </ul>
              )
            }})()}
            </li>
            )
  }
}

function mapState(state){
  return {
    skillIcon: state.skillIcon,
  };
}
function mapDispatch(dispatch){
  return bindActionCreators({requestSkillIcons},dispatch)
}
export default connect(mapState,mapDispatch)(ExpIcons);