import React from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { requestCareerList , setCareerInfo ,setCompanyInfo } from '../../../actions/index';
import { bindActionCreators } from 'redux';
import $ from 'jquery';

class TimeLine extends React.Component {
  constructor(props) {
    super(props);
    this.cilckhandler=this.cilckhandler.bind(this);
  }
  componentWillMount() {
    this.props.requestCareerList('kaoru');
  } 
  componentDidUpdate(prevProps, prevState) {
    var listnum = this.props.careerList.length-1;
    this.cilckhandler(this.props.careerList[listnum],listnum);
  }
  cilckhandler(item,index){
    this.setInfo(item);
    var ulDOM = ReactDOM.findDOMNode(this);
    $(ulDOM).find('li').removeClass('active');
     $(ulDOM).find('li').eq(index).addClass('active');
  }
  setInfo(item){
    this.props.setCareerInfo(item);
    this.props.setCompanyInfo(item.company);
  }
  render_list(){
    if(this.props.careerList){
    return this.props.careerList.map((item,index)=>{
      return(
        <li key={item.title} onClick={()=>{ this.cilckhandler(item,index)}} ><b>{item.endYears}</b><i></i><span>{item.title}</span></li>
        );
    });
    }
  }
  render() {
    return (
            <ul className="timeLine_ul">
              {this.render_list()}
            </ul>        
          );
  }
}
function mapStateToProps(state){
  return {
    careerList:state.careerList
  }
}
function mapDispatchToProps(dispatch){
  return bindActionCreators({requestCareerList,setCareerInfo,setCompanyInfo},dispatch);
}
export default connect(mapStateToProps,mapDispatchToProps)(TimeLine);
