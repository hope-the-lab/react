import React from 'react';
import TimeLine from './TimeLine';
import CareerInfo from './CareerInfo';
import CompanyInfo from './CompanyInfo';
import PrductInfo from './PrductInfo';

import myEvent from '../../../actions/event';

export default class Career extends React.Component {
  render() {
    return (
      <section className="profile_career">
            <div className="leftsidepadding"></div>
            <div className="content">
              <div className="flex_half">
                <div className="left_block">
                  <img className="img_title" src="/img/title_career.png" />
                  <div>
                    <article>
                      <TimeLine />
                    </article>
                  </div>
                </div>
              </div>
              <div className="flex_half">
                <div className="right_block">
                  <CompanyInfo />
                  <CareerInfo />
                  <PrductInfo />
                </div>
              </div>
            </div>
            <div className="rightsidepadding"></div>
          </section>
    );
  }
}
