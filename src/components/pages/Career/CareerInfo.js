import React from 'react';
import ReactDom from 'react-dom';
import {connect} from 'react-redux';
import {requestSkillIcons} from '../../../actions/index';
import {bindActionCreators} from 'redux';
import {TweenLite} from 'gsap';

import ExpIcons from './ExpIcons';

class CareerInfo extends React.Component {
  translateHtml(string){
    return({__html:string});
  }
  componentWillReceiveProps(nextProps) {
    var dom = ReactDom.findDOMNode(this);
    TweenLite.to(dom,0,{opacity:0,transform: 'rotateY(30deg)',delay:0});

  }
  componentDidUpdate(prevProps, prevState) {
    var dom = ReactDom.findDOMNode(this);
    if(this.props.careerInfo){
      this.props.requestSkillIcons(this.props.SkillData,this.props.careerInfo.experience);
    }
    TweenLite.to(dom,0.5,{opacity:1,transform: 'rotateY(0deg)',delay:0.3});
  }
  render() {
    var info = this.props.careerInfo;
    if(info){
          return (
            <div className="company_info">
              <article>
                <h3>{info.startYears}{(()=>info.startYears?'-':'')()}{info.endYears}</h3>
                <ul className="info_ul">
                  <li style={(!info.jobTitle)?{display:'none'}:{}}><ul><li>JOB TITLE</li><li>{info.jobTitle}</li></ul></li>
                  <ExpIcons />
                  <li style={(!info.description)?{display:'none'}:{}}>
                    <ul>
                      <li>Description</li><li dangerouslySetInnerHTML={this.translateHtml(info.description)} ></li>
                    </ul>
                  </li>
                </ul>
              </article>
            </div>
          );
    }else{
      return (<i></i>);
    }
      
  }
}

function mapState(state){
  return {
    careerInfo: state.careerInfo,
    SkillData: state.SkillData,
  };
}
function mapDispatch(dispatch){
  return bindActionCreators({requestSkillIcons},dispatch)
}
export default connect(mapState,mapDispatch)(CareerInfo);