import React from 'react';
import {connect} from 'react-redux';
import ReactDom from 'react-dom';
import {TweenLite} from 'gsap';
import {bindActionCreators} from 'redux';
import {requestPopUp} from '../../../actions/index';

import myEvent from '../../../actions/event';

class PrductInfo extends React.Component {
  constructor(props) {
    super(props);
    this.sreachByCompany=this.sreachByCompany.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    var dom = ReactDom.findDOMNode(this);
    TweenLite.to(dom,0,{opacity:0,transform: 'rotateY(30deg)',delay:0});
  }
  componentDidUpdate(prevProps, prevState) {
    var dom = ReactDom.findDOMNode(this);
    TweenLite.to(dom,0.5,{opacity:1,transform: 'rotateY(0deg)',delay:0.1});
    //console.log(this.props.companyInfo);
  }
  clickHandler(data){
    this.props.requestPopUp(data);
    myEvent.emit('showPop');
  }
  renderList(product){
    var base = this;
    return(
      product.map(function(pid){
        var item = base.props.collectData[pid];
        return(
        <div key={pid} className="col_half">
          <div className="collections_item" onClick={()=>{base.clickHandler(item)}} style={{backgroundImage:'url(img/'+item.artist_id+'/collection/'+item.tmb_img+')'}}>
            <div className="layout">
              <label>{item.kind}</label>
            </div>
          </div>
        </div>
      )
      })
    )
  }
  sreachByCompany(comID){
    var data = {
      title:'organization',
      value:comID
    }
    myEvent.emit('searchFromRemote',data);
  }
  render() {
    var info = this.props.companyInfo||false;
    if(info.product){
      return (
      <div className="relative_product">
        <div><b>product</b><span onClick={()=>this.sreachByCompany(info.id)}>more</span><i className="fa fa-caret-right" aria-hidden="true"></i></div>
        <article>
          <div className="clearfix">
          {this.renderList(info.product)}
          </div>
        </article>
      </div>           
      )
    }else{
      return (
        <div className="relative_product"></div>
      )
    }
  }
}

function mapState(state){
  return {
    companyInfo: state.companyInfo,
    collectData: state.collectData,
  };
}
function mapDispatcher(dispatcher){
  return bindActionCreators({requestPopUp},dispatcher)
}

export default connect(mapState,mapDispatcher)(PrductInfo);