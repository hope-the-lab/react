import React from 'react';
import {connect} from 'react-redux';
import ReactDom from 'react-dom';
import {TweenLite} from 'gsap';

class CompanyInfo extends React.Component {

  componentWillReceiveProps(nextProps) {
    var dom = ReactDom.findDOMNode(this);
    TweenLite.to(dom,0,{opacity:0,transform: 'rotateY(30deg)',delay:0});
  }
  componentDidUpdate(prevProps, prevState) {
    var dom = ReactDom.findDOMNode(this);
    TweenLite.to(dom,0.5,{opacity:1,transform: 'rotateY(0deg)',delay:0.5});
  }
  translateHtml(string){
    return({__html:string});
  }
  render() {
    var info = this.props.companyInfo||false;
    if(info){
          return (
            <div className="company">
              <article>
              {(()=>{
                if(info.img){return(
                <div className="logo_block">
                  <img src={'img/career/'+info.img} />
                </div>
                )}else{
                  return(
                    <div className="logo_block">
                    <h2>{info.name}</h2>
                    <h3>{info.name_ch}</h3>
                    </div>
                    );
                }
              })()}
                <p dangerouslySetInnerHTML={this.translateHtml(info.description)}></p>
              </article>
            </div>
          );
    }else{
      return (<div className="company"></div>);
    }
  }
}

function mapState(state){
  return {
    companyInfo: state.companyInfo,
  };
}

export default connect(mapState)(CompanyInfo);