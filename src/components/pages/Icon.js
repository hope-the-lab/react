import React from 'react';
import ReactDOM from 'react-dom';
import myEvent from '../../actions/event';
import $ from 'jquery';

export default class Icon extends React.Component {
  constructor(props) {
    super(props);
    this.clickhandler=this.clickhandler.bind(this)
  }
componentDidMount() {
  var dom = ReactDOM.findDOMNode(this);
  $(dom).hover(
    function(){
      $(this).addClass('active');
  },function(){
      $(this).removeClass('active');
  })
}
clickhandler(){
  var base = this;
  var data = {
      title:'skill',
      value:base.props.id,
    }
  myEvent.emit('searchFromRemote',data);
}
render() {
  return(
    <span className="componentIcon" onClick={this.clickhandler}>
      <img alt={this.props.alt} src={this.props.src} style={(this.props.level<1)?{opacity:0.5}:{}} />
      <label>
        <span>{this.props.alt}</span>
        {(this.props.level<1)?<p><span>not familiar</span></p>:''}
      </label>
    </span>
  )
  }
}