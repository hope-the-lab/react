import React from 'react';
import {TweenLite} from 'gsap';
import ReactDOM from 'react-dom';

export default class BackgroundCover extends React.Component {
  constructor(props) {
    super(props);
    var colorReferrence = ['rgba(0,0,0,0)','#F7931E','#39B54A','#29ABE2','#FF0000'];
    var totalPage=colorReferrence.length;
    this.state={
      nowPage:0,
      colorReferrence,
    }
  }
  componentWillReceiveProps(nextProps) {
    var newPage = nextProps.nowPage;
    if(newPage!=this.state.nowPage && newPage >= 0){
      this.pageHendler(newPage);
    }
  }
  pageHendler(newPage){
    var parentDom = ReactDOM.findDOMNode(this);
    var dom = parentDom.childNodes;
    this.setState({nowPage:newPage});
    TweenLite.to(dom,0.3,{backgroundColor:this.state.colorReferrence[newPage]});
    if(newPage==0){
      TweenLite.to(parentDom,0.3,{backgroundColor:'rbga(0,0,0,0)'});
    }else{
      TweenLite.to(parentDom,0.3,{backgroundColor:'#fff'});
    }
    
  }

  render() {
    return (
          <div className="cover">
            <div className="insideCover"></div>
          </div>
    );
  }
}
