import React from 'react';
import {TweenLite , CSSRulePlugin } from 'gsap';
import {connect} from 'react-redux';
import myEvent from '../../actions/event';
import YouTube from 'react-youtube';
import Icon from './Icon'
class PopUp extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      isOpen:false,
    }
    this.openhandler=this.openhandler.bind(this);
  }
  componentDidMount() {
    var base =this;
    var elTarget = document.querySelector('#popUpContainer')
    elTarget.onclick=function(event){
      if(event.target == elTarget){
        base.setState({isOpen:false});
      }
    };
    document.querySelector('#closeNav').onclick=function(){
      base.setState({isOpen:false});
    };

    myEvent.on('showPop',this.openhandler);
  }
  openhandler() {
    this.setState({isOpen:true});
  }
  renderIcon(icons){
    var base = this;
    return(
      icons.map(function(item){
        if (base.props.IconData[item]) {
          return(
            <span  key={base.props.IconData[item].name}  onClick={()=>base.setState({isOpen:false})}>
              <Icon alt={base.props.IconData[item].name} id={base.props.IconData[item].id} level={base.props.IconData[item].level} src={'img/icons/'+base.props.IconData[item].img} />
            </span>
          )
        }else{
          return(
            <span>{item}</span>
          )
        }
      })
    )
  }
  renderSlides(artist,imgs){
    if(this.state.isOpen){
        if(Array.isArray(imgs)){
          return(
            imgs.map(function(item){
              return (
                <li key={item} style={{backgroundImage:'url(img/'+artist+'/collection/'+item+')'}}></li>
              )
            })
          )
        }else{
          var array = Object.keys(imgs);
          return(
            array.map(function(item){
            switch (item){
              case 'video' :
              console.log();
                return (
                  <li key={imgs[item]} className="video">
                    <YouTube videoId={imgs[item]} />
                    }
                  </li>
                  );
              case 'website' :
                return (
                  imgs[item].img.map(function(img){
                    return (
                      <li key={img} style={{backgroundImage:'url(img/'+artist+'/collection/'+img+')'}}>
                        <a className="fullBlock" href={imgs[item].href} target="_blank"></a>
                      </li>
                    )
                  })
                )
            }
          })
          )
        }
    }
  }
  translateHtml(string){
    return({__html:string});
  }
  closeNav(isOpen){
    if(isOpen){
      TweenLite.to('#closeNav',0.5,{css:{className:'isopen'},delay:0.5});
    }else{
      TweenLite.to('#closeNav',0.5,{css:{className:''}});
    }
  }
  frameAnimate(isOpen){
    if(isOpen){
      TweenLite.to('#popUpContainer .frame',0.7,{transform:'scale(1)',opacity:1});
    }else{
      TweenLite.to('#popUpContainer .frame',0.8,{transform:'scale(0.5)rotateX(31deg)translateZ(10rem)',opacity:0});
    }
  }
  popBgAnimate(isOpen){
    if(isOpen){
      TweenLite.to('#popUpContainer',1,{opacity:1});
      document.querySelector('#popUpContainer').style.pointerEvents = 'all';
    }else{
      TweenLite.to('#popUpContainer',0.5,{opacity:0});
      document.querySelector('#popUpContainer').style.pointerEvents = 'none';
    }
  }
  render() {
    if(this.props.PopUp){
      var pop = this.props.PopUp;
      {this.frameAnimate(this.state.isOpen)}
      {this.popBgAnimate(this.state.isOpen)}
      return (
            <div id="popUpContainer">
              <div className="frame">
                <nav id="closeNav">{this.closeNav(this.state.isOpen)}</nav>
                <div className="content">
                  <div className="info_block">
                    <h2>{pop.name}</h2>
                    <h3>{pop.publish_date}</h3>
                    <div className="img_block">{this.renderIcon(pop.skill)}</div>
                    <ul>
                      <li>Description</li>
                      <li dangerouslySetInnerHTML={this.translateHtml(pop.intro)}></li>
                    </ul>
                  </div>
                  <div className="slide_block">
                    <div className="flexslider">
                      <ul className="slides">
                        {this.renderSlides(pop.artist_id,pop.img)}
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            );
    }else{
      return(
        <div id="popUpContainer">
              <div className="frame">
                <nav id="closeNav"></nav>
                <div className="content">
                  <div className="info_block">
                    <h2></h2>
                    <h3></h3>
                    <div className="img_block"></div>
                    <ul>
                      <li>Description</li>
                      <li></li>
                    </ul>
                  </div>
                  <div className="slide_block">
                    <div className="flexslider">
                      <ul>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
        </div>
      );
    }
    
  }
}
function mapState(state){
  return {
    PopUp:state.collectPopUp,
    IconData:state.SkillData,
  }
}
export default connect(mapState)(PopUp)