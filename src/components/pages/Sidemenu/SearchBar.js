import React from 'react';
import {TweenLite} from 'gsap';
import ReactDOM from 'react-dom';
import $ from 'jquery';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {requestCollectRecommend,requestCollectBySearch,searchBar} from '../../../actions/index';
import myEvent from '../../../actions/event';

var searchKey={};
class SearchBar extends React.Component {
  constructor(props) {
    super(props);
    this.changeHandler=this.changeHandler.bind(this);
    this.resetSearch=this.resetSearch.bind(this);
    this.remoteSearch=this.remoteSearch.bind(this);
  }
  componentWillMount() {
    // var ids =['k001','k010','k005','k007','k015'];
    //this.props.resRecommend(this.props.collectData,ids);
    this.props.searchBar(this.props.collectData);
    this.props.resSreach(this.props.collectData,searchKey);
  }
  componentDidMount() {
    myEvent.on('searchReset',this.resetSearch);
    myEvent.on('searchFromRemote',this.remoteSearch);
  }
  renderOptionList(name){
    if(this.props.collectSreachBar){
      var array = this.props.collectSreachBar[name];
      return(
        array.map(function(value){
          return (
            <option key={value} value={value}>{value}</option>
          )
        })
      )
    }
  }
  renderSkillList(){
    var base = this;
    if(this.props.collectSreachBar){
      var array = this.props.collectSreachBar['skill'];
      return(
        array.map(function(value){
          var skill = (base.props.SkillData[value])?base.props.SkillData[value].name:value;
          return (
            <option key={value} value={value} >{skill}</option>
          )
        })
      )
    }
  }
  changeHandler(event){
    searchKey[event.target.title]=event.target.value;
    this.props.resSreach(this.props.collectData,searchKey);
    myEvent.emit('goToPageOfTop');
  }
  resetSearch(){
    searchKey={};
    var dom = ReactDOM.findDOMNode(this);
    $(dom).find('option[value="all"]').prop({'selected':true});
    this.props.resSreach(this.props.collectData,searchKey);
  }
  remoteSearch(data){
    searchKey={};
    var dom = ReactDOM.findDOMNode(this);
    $(dom).find('option[value="all"]').prop({'selected':true});
    $(dom).find('option[value="'+data.value+'"]').prop({'selected':true});
    searchKey[data.title]=data.value;
    this.props.resSreach(this.props.collectData,searchKey);
    var toPage=3;
    myEvent.emit('goToPage',toPage);
    myEvent.emit('goToPageOfTop');
  }
  render() {
    return (
          <ul>
            <li><span>Years</span>
                <select onChange={this.changeHandler} title="publish_date">
                <option value="all">All</option>{this.renderOptionList('publish_date')}
                </select></li>
            <li><span>Skill</span>
                <select onChange={this.changeHandler} title="skill">
                <option value="all">All</option>{this.renderSkillList('skill')}
                </select></li>
            <li><span>Organization</span>
                <select onChange={this.changeHandler} title="organization">
                <option value="all">All</option>{this.renderOptionList('organization')}
                </select></li>
            <li><span>Category</span>
                <select onChange={this.changeHandler} title="kind">
                <option value="all">All</option>{this.renderOptionList('kind')}
                </select></li>
          </ul>          
    );
  }
}

function mapState(state){
  return {
    collectSreachBar:state.collectSreachBar,
    collectData:state.collectData,
    SkillData:state.SkillData,
  }
}

function mapDispatcher(dispatcher){
  return bindActionCreators({
    resRecommend:requestCollectRecommend,
    resSreach:requestCollectBySearch,
    searchBar},dispatcher)
}

export default connect(mapState,mapDispatcher)(SearchBar)