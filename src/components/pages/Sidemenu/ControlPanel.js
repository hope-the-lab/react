import React from 'react';
import {TweenLite} from 'gsap';
import ReactDOM from 'react-dom';
import $ from 'jquery';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {requestCollectRecommend,requestCollectBySearch,searchBar} from '../../../actions/index';
import SearchBar from './SearchBar';

import myEvent from '../../../actions/event';
class ControlPanel extends React.Component {
  constructor(props) {
    super(props);
    this.goPage=this.goPage.bind(this);
    this.toggoleSreach=this.toggoleSreach.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    var dom = ReactDOM.findDOMNode(this);
    if(nextProps.isOPEN){
      TweenLite.to(dom,1,{transform:'rotateX(0deg)'});
    }else{
      TweenLite.to(dom,0.5,{transform:'rotateX(-90deg)'});
    }
    this.toggoleSreach(nextProps.isSEARCH);
  }
  toggoleSreach(isSEARCH){
    var dom = ReactDOM.findDOMNode(this);
    var width = $(dom).width()/2;
    if(isSEARCH){
      TweenLite.to(dom.childNodes[0],0.5,{transform:'rotateY(-90deg)translateZ('+width+'px)translateX(-'+(width*2)+'px)',zIndex:0});
      TweenLite.to(dom.childNodes[1],0.5,{transform:'translateZ(0)',opacity:1});
    }else{
      TweenLite.to(dom.childNodes[0],0.5,{transform:'translateZ(0)'});
      TweenLite.to(dom.childNodes[1],0.5,{transform:'rotateY(90deg)translateZ('+width+'px)',opacity:0});
    }
  }
  goPage(int){
    myEvent.emit('goToPage',int);
  }
  render() {
    return (
          <div id="controlPanel">
            <div className="selector">
              <ul>
                <li onClick={()=>this.goPage(1)}><i></i>About</li>
                <li onClick={()=>this.goPage(2)}><i></i>Career</li>
                <li onClick={()=>this.goPage(3)}><i></i>Collections</li>
                <li onClick={()=>this.goPage(4)}><i></i>Contact</li>
              </ul>
            </div>
            <div className="searcher">
              <div>
                <h3 onClick={()=>this.toggoleSreach(false)}><i className="fa fa-caret-left" aria-hidden="true"></i>back</h3>
                <hr />
                <h4>search<i className="fa fa-search" aria-hidden="true"></i>by</h4>
                <SearchBar/>
              </div>
            </div>
          </div>
    );
  }
}

function mapState(state){
  return {
    collectData:state.collectData,
  }
}

function mapDispatcher(dispatcher){
  return bindActionCreators({resRecommend:requestCollectRecommend,resSreach:requestCollectBySearch,searchBar},dispatcher)
}

export default connect(mapState,mapDispatcher)(ControlPanel);