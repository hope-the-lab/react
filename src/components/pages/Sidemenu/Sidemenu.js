import React from 'react';
import {TweenLite} from 'gsap';
import ReactDOM from 'react-dom';

import ControlPanel from './ControlPanel';
import ContactIocnList from '../Contact/ContactIocnList';

export default class Sidemenu extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      nowIndex:true,
      isOPEN:false,
      isSEARCH:false,
    }
    this.nowIndex = true;
  }
  componentDidMount() {
    
  }
  componentWillReceiveProps(nextProps) {
    if(nextProps.nowPage>0){
      if (this.nowIndex) {
        this.animate_next();
        this.nowIndex=false;
      }
      }else{
        if (!this.nowIndex) {
          this.animate_prev();
          this.nowIndex=true;
        }
      }
    if(nextProps.nowPage==3){
      this.setState({isSEARCH:true});
    }else{
      if(this.state.isSEARCH){
      this.setState({isSEARCH:false});
      }
    }
  }
  animate_next(){
    var base = this;
    var parentDom = ReactDOM.findDOMNode(this);
    if(!this.props.isMobile){
      TweenLite.to(parentDom,0.1,{opacity:0,onComplete:function(){
        TweenLite.to(parentDom,0,{className:'sidemenuSideMode',onComplete:function(){
          TweenLite.to(parentDom,0.5,{opacity:1});
          base.setState({isOPEN:true});
        }});
      }});
    }else{
      parentDom.setAttribute('class','sidemenuSideMode');
    }

  }
  animate_prev(){
    var base = this;
    var parentDom = ReactDOM.findDOMNode(this);
    if(!this.props.isMobile){
    TweenLite.to(parentDom,1,{className:'sidemenuCoverMode'});
    base.setState({isOPEN:false});
    }else{
      parentDom.setAttribute('class','sidemenuCoverMode');
    }
  }

  render() {
    return (
          <div id="sidemenu" className="sidemenuCoverMode">
            <div id="informationBlock">
              <img className="logo" src="img/lab_logo.png" />
              <nav className="crossBox"></nav>
              <img className="signal" src="img/signal.png" />
              <div className="infoCard">
                <h3>Isaac<span>(kaoru)</span></h3>
                <h4>Front-end Developer</h4>
                <ContactIocnList />
              </div>
              <div className="scrollDown">
                <h4>Scroll</h4>
                <i className="fa fa-angle-down" aria-hidden="true"></i>
              </div>
            </div>
            <ControlPanel isOPEN={this.state.isOPEN} isSEARCH={this.state.isSEARCH}/>
          </div>
    );
  }
}
