import React from 'react';
import YouTube from 'react-youtube';


const opts = {
      height: '287',
      width: '510',
      playerVars: { // https://developers.google.com/youtube/player_parameters 
        autoplay: 1,
        controls: 0,
        loop:1,
        showinfo:0,
        list:'PLfmSmPVAOgX96rl3ZPdYApY0iq7u-g7Q6',
        listType:'playlist',
      }
    };

export default class About extends React.Component {
  render() {
    return (
      <section className="profile_about">
            <div className="leftsidepadding"></div>
            <div className="content">
              <img className="img_title" src="img/title_about.png" />
              <div className="content_block">
                <div className="background"></div>
                <div className="layout clearfix">
                  <div className="photo_block">
                    <div className="mask"><img src="img/about_pic.jpg" /></div>
                    <div className="small_pic_block">
                      <img src="img/small_pic1.jpg" />
                      <img src="img/small_pic2.jpg" />
                      <img src="img/small_pic3.jpg" />
                    </div>
                  </div>
                  <div className="article_block">
                    <article>
                      <h1>Isaac Lin<span>林士薰</span></h1>
                      <p>
                        Isaac shows his passion for doing creative things since he was sixth grades student researching about online game server,  drawing 2D & 3D animation by Maya or Flash when middle school, running a rock band when high school. coding Java, Javascript, HTML, Processing things  in college. All of these about sharing his little thought in his mind .
                      </p>
                      <hr />
                      <p className="major">HTML5, CSS3, JQuery, Javascript, Photoshop, Illustrator, NodeJS</p>
                      <p className="sub">MangoDB, Angular, React, React-Native, Java, Maya, Soildwork</p>
                      <div className="youtube_block">
                        <div className="layout">
                          <img src="img/youtube.png" />
                          <object>
                            {(!this.props.isMobile)?(<YouTube videoId="zewaof3uUjY" opts={opts} />):<img src="img/youtube.jpg" />}
                            <nav className="right"></nav>
                            <nav className="left"></nav>
                          </object>
                        </div>
                      </div>
                    </article>
                  </div>
                </div>
              </div>
            </div>
            <div className="rightsidepadding"></div>
          </section>
    );
  }
}
