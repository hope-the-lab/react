/*require('../css/normal.css');
require('../css/style.css'); */
import $ from 'jquery';
import {TweenLite} from 'gsap';

import React from 'react';
import Header from './pages/Header/Header';
import Sidemenu from './pages/Sidemenu/Sidemenu';
import About from './pages/About/About';
import Career from './pages/Career/Career';
import Collections from './pages/Collections/Collections';
import Contact from './pages/Contact/Contact';

import BackgroundCover from './pages/BackgroundCover';
import PopUp from './pages/PopUp';

import myEvent from '../actions/event';

require('../lib/smoothwheel');

class AppComponent extends React.Component {
  constructor() {
    super();
    var w_height = window.innerHeight;
    this.state={
      w_height:w_height,
      w_scrollTop:0,
      height_array:[],
      nowPage:0,
      lockFindPage:false,
      isMobile:false,
    }
    this.scrollHandler = this.scrollHandler.bind(this);
    this.resizeHandlerHandler = this.resizeHandlerHandler.bind(this);
    this.findNowPage=this.findNowPage.bind(this);
    this.collectHeight=this.collectHeight.bind(this);
    this.jumpToPage=this.jumpToPage.bind(this);
    this.pageScrollTopAnimate=this.pageScrollTopAnimate.bind(this);
  }
  componentDidMount() {
    $(this.w_scroll).on('scroll',(event)=>{this.scrollHandler(event)});
    $(window).on('resize',(event)=>{this.resizeHandlerHandler(event)});
    if($(window).width()<768){
      this.setState({isMobile:true});
    }

    this.collectHeight();
    myEvent.on('reTotalHeight',this.collectHeight);
    myEvent.on('goToPage',this.jumpToPage);
    myEvent.on('goToPageOfTop',this.pageScrollTopAnimate);
  }
  componentWillUnmount() {
    $(this.w_scroll).unbind('scroll');
    myEvent.removeListener('reTotalHeight',this.collectHeight);
    myEvent.removeListener('goToPage',this.jumpToPage);
    myEvent.removeListener('goToPageOfTop',this.pageScrollTopAnimate);
    $(window).unbind('resize');
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevState.nowPage != this.state.nowPage&&!this.state.isMobile) {
      this.pageScrollTopAnimate();
      this.pageFoucus(this.state.nowPage);
    }
  }
  scrollHandler(event){
    var w_scrollTop = event.target.scrollTop;
    this.setState({w_scrollTop});
    if(!this.state.lockFindPage){
      this.setState({nowPage:this.findNowPage()});
    }
  }
  resizeHandlerHandler(event){
    this.changeWHeight(event.target.innerHeight);
  }
  changeWHeight(newHeight){
    this.setState({w_height:newHeight});
    this.collectHeight();
  }
  collectHeight(){
    var height_array = (function(){
      var tmp_arry =[]
      var mixHeight = 0;
      $('.contentContainer > section').each(function(){
        mixHeight += $(this).height()+100;
        tmp_arry.push(mixHeight);
      });
      return tmp_arry;
    })()
    this.setState({height_array});
  }
  jumpToPage(goPage){
    this.setState({nowPage:goPage});
  }
  pageScrollTopAnimate(){
      var base = this;
      var scrollto = base.state.height_array[base.state.nowPage-1]||0;
      TweenLite.to('#WrapperContainer',0.8,{
          onStart:function(){
            base.setState({lockFindPage:true});
          },
          onComplete:function(){
            base.setState({lockFindPage:false});
          },
          scrollTop:(scrollto-40)
        });
  }
  findNowPage(){
    var base =this;
    var testHeight = 0;
    var pageNum =this.state.height_array.length;
    for(var i = 0;i < pageNum ;i++){
      if(base.state.w_scrollTop+200 < base.state.height_array[i]){
        return i;
      }
    }
  }
  pageFoucus(foucusPage){
    $('.contentContainer > section').each(function(index){
          if(foucusPage!=index){
            TweenLite.to(this,0.2,{opacity:0.5});
          }else{
            TweenLite.to(this,1,{opacity:1});
          }
      });
  }



  render() {
    return (
      <div id="BackgroundWrap" style={{height:this.state.w_height}}>
        <BackgroundCover nowPage={this.state.nowPage} w_height={this.state.w_height} />
        <div id="Wrapper">
          <div id="WrapperContainer" ref={(ref) => this.w_scroll = ref}>
            <div className="contentContainer">
              <Header></Header>
              <Sidemenu nowPage={this.state.nowPage} isMobile={this.state.isMobile} height_array={this.state.height_array}></Sidemenu>
              <section style={{minHeight:this.state.w_height}}></section>
              <About isMobile={this.state.isMobile}></About>
              <Career></Career>
              <Collections w_height={this.state.w_height}></Collections>
              <Contact w_height={this.state.w_height}></Contact>
            </div>
          </div>
          <PopUp/>
        </div>
      </div>
    );
  }
}

AppComponent.defaultProps = {
};

export default AppComponent;
