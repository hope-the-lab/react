export function requestContactIcon(userId) {
  return {
    type: 'REQUEST_CONTACT_ICON',
    userId,
  };
}
export function requestCareerList(userId) {
  return {
    type: 'REQUEST_CAREER_LIST',
    userId,
  };
}
export function setCareerInfo(info) {
  return {
    type: 'SET_CAREER_INFO',
    info,
  };
}
export function setCompanyInfo(company) {
  return {
    type: 'SET_COMPANY_INFO',
    company,
  };
}
export function requestSkillIcons(data,icons) {
  return {
    type: 'REQUEST_SKILL_ICON',
    icons,
    data,
  };
}
export function requestCollectRecommend(data,ids) {
  return {
    type: 'REQUEST_COLLECTION_RECOMMEND',
    data,
    ids,
  };
}
export function requestCollectBySearch(data,keypack) {
  return {
    type: 'REQUEST_COLLECTION_BY_SEARCH',
    data,
    keypack,
  };
}
export function searchBar(data) {
  return {
    type: 'SEARCH_BAR',
    data,
  };
}
export function requestPopUp(data) {
  return {
    type: 'REQUEST_POP_UP',
    data,
  };
}